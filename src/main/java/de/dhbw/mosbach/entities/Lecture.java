package de.dhbw.mosbach.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Created by Waldemar on 21.02.2016.
 */
@Entity
public class Lecture implements Serializable {

    public Lecture(){
        // used for JPA
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer uid;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endTime;
    // Create or last modified time
    @Version
    @JsonIgnore
    private LocalDateTime lastModified;
    private String professor;
    private String lectureName;
    private String location;
    // @JsonIgnore
    private String course;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLectureName() {
        return lectureName;
    }

    public void setLectureName(String lectureName) {
        this.lectureName = lectureName;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public LocalDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalDateTime endTime) {
        this.endTime = endTime;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Lecture lecture = (Lecture) o;

        return new EqualsBuilder()
                .append(this.startTime, lecture.startTime)
                .append(this.endTime, lecture.endTime)
                .append(this.location, lecture.location)
                .append(this.professor, lecture.professor)
                .append(this.lectureName, lecture.lectureName)
                .append(this.course, lecture.course)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(5, 13)
                .append(startTime)
                .append(endTime)
                .append(location)
                .append(professor)
                .append(lectureName)
                .append(course)
                .toHashCode();
    }
}
