package de.dhbw.mosbach.services;

import biweekly.Biweekly;
import biweekly.ICalendar;
import biweekly.component.VEvent;
import biweekly.property.DateOrDateTimeProperty;
import biweekly.property.TextProperty;
import com.google.common.collect.Sets;
import de.dhbw.mosbach.entities.Course;
import de.dhbw.mosbach.entities.Lecture;
import de.dhbw.mosbach.repositories.LectureRepository;
import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Waldemar on 21.02.2016.
 */
@Service
@Async
public class LectureService {

    @Inject
    private LectureRepository lectureRepository;
    private AtomicInteger id = new AtomicInteger();

    @Transactional
    public Future<String> loadLectures(Course course) {
        // System.out.println("Startet Sync for " + course.getCourseKey());
        // long startTime = System.currentTimeMillis();
        Set<Lecture> oldLectures = lectureRepository.getByCourse(course.getCourseKey());
        File tempFile = new File(course.getCourseKey());
        ICalendar iCalendar;
        try {
            FileUtils.copyURLToFile(course.getCalenderURL(), tempFile);
            iCalendar = Biweekly.parse(tempFile).first();
            tempFile.delete();
        } catch (IOException e) {
            System.out.println("Failed to download calender for " + course.getCourseKey());
            return new AsyncResult<>(course.getCourseKey());
        }
        Set<Lecture> newLectures = Sets.newHashSet();

        for (VEvent event : iCalendar.getEvents()) {
            Lecture lecture = new Lecture();
            lecture.setCourse(course.getCourseKey());
            lecture.setLectureName(getSaveValue(event.getSummary()));
            lecture.setProfessor(getSaveValue(event.getDescription()));
            lecture.setLocation(getSaveValue(event.getLocation()));
            lecture.setStartTime(parseDate(event.getDateStart()));
            lecture.setEndTime(parseDate(event.getDateEnd()));
            if (!StringUtils.isEmpty(lecture.getLectureName()))
                newLectures.add(lecture);
        }

        // 1. Delete modified or deleted lectures.
        lectureRepository.delete(Sets.difference(oldLectures, newLectures));
        // 2. Save new added lectures.
        lectureRepository.save(Sets.difference(newLectures, oldLectures));
//        long endTime = System.currentTimeMillis();
//        long duration = (endTime - startTime);
//        System.out.println(id.incrementAndGet() + " Synchronized oldLectures for " + course.getCourseKey() + " in " + duration + " ms!");
        return new AsyncResult<>(course.getCourseKey());
    }

    private LocalDateTime parseDate(DateOrDateTimeProperty date) {
        return LocalDateTime.ofInstant(date.getValue().getRawComponents().toDate().toInstant(), ZoneId.systemDefault());
    }

    private String getSaveValue(TextProperty property){
        return property != null ? property.getValue() : "";
    }
}
