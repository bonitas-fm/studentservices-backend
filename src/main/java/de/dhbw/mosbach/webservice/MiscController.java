package de.dhbw.mosbach.webservice;

import de.dhbw.mosbach.repositories.LectureRepository;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * Created by Waldemar on 16.03.2016.
 */

@SuppressWarnings("unused")
@RestController
public class MiscController {

    @Inject
    private LectureRepository lectureRepository;
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");


    @RequestMapping(path = "/rooms", method = RequestMethod.GET, produces = "application/json")
    public Set<String> getFreeRooms(
            @RequestParam String start,
            @RequestParam String end
    ) {
        Set<String> allRooms = lectureRepository.getAllRooms();
        Set<String> reservedRooms = lectureRepository.getReservedRooms(LocalDateTime.parse(start, formatter), LocalDateTime.parse(end, formatter));
        allRooms.removeAll(reservedRooms);
        return allRooms;
    }
}
